Madrona Total War by Eremanth
Unoficial submod of TATW Total War by King Kong and his team

Special thanks to :
    - TATW team
    - Gigantus and his Bare Geomod
    - Mundus Bellicus community
    - LLDC background's authors
    - LLDC dev team

FEATURES:
    - New map based on LLDC background
    - New factions based on LLDC background

LINKS:
TATW - http://www.twcenter.net/forums/forumdisplay.php?654-Third-Age-Total-War
Bare Geomod - http://www.twcenter.net/forums/showthread.php?352216-Creating-a-World-Bare-Geomod
LLDC - http://www.lldc.fr