;
; regions list
;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Amneroth_Province
	Amneroth
	england
	English_Rebels
	196 23 23
	no_brigands, no_pirates
	8
	2
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Tour_Nord_Province
	Tour_Nord
	england
	English_Rebels
	23 43 196
	no_brigands, no_pirates
	3
	2
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Tour_Sud_Province
	Tour_Sud
	england
	English_Rebels
	112 116 143
	no_brigands, no_pirates
	3
	2
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Tour_Est_Province
	Tour_Est
	england
	English_Rebels
	184 192 241
	no_brigands, no_pirates
	3
	2
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Tour_Ouest_Province
	Tour_Ouest
	england
	English_Rebels
	143 151 212
	no_brigands, no_pirates
	3
	2
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Montagnes_Blanches
	Bhagabor
	hre
	English_Rebels
	229 211 14
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Terres_des_Geants
	Mont_Cannipent
	spain
	English_Rebels
	123 154 141
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Er_Nathar
	Ker_Nather
	venice
	English_Rebels
	106 107 68
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Levant_Province
	Naer_Lorien
	france
	English_Rebels
	37 102 12
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Plaines_de_Rhajad
	Zul_Rhork
	milan
	English_Rebels
	115 220 46
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Marais_Arhun
	Echteliarn
	sicily
	English_Rebels
	169 174 36
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Plaines_de_Landraska
	Bagran
	scotland
	English_Rebels
	43 221 253
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Gaal_Province
	Zul_Gaal
	sicily
	English_Rebels
	130 198 208
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Coss_Province
	Lan_Coss
	sicily
	English_Rebels
	253 137 238
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Lun_Province
	Der_Lun
	sicily
	English_Rebels
	253 137 30
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Garig_Province
	Garig
	sicily
	English_Rebels
	179 120 120
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Ellanterm_Province
	Ellanterm
	france
	English_Rebels
	176 217 160
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Gran_Large_Province
	Gran_Large
	sicily
	English_Rebels
	220 224 114
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Gran_Garig_Province
	Gran_Garig
	sicily
	English_Rebels
	125 128 52
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Port_Caracol_Province
	Port_Caracol
	sicily
	English_Rebels
	47 40 193
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Etten_Province
	Etten
	sicily
	English_Rebels
	140 24 188
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Bor_Lac_Province
	Bor_Lac
	sicily
	English_Rebels
	72 166 103
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Gran_Mnyop_Province
	Gran_Mnyop
	france
	English_Rebels
	104 97 198
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Irzul_Province
	Irzul
	france
	English_Rebels
	148 123 154
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Lamenui_Province
	Lamenui
	france
	English_Rebels
	124 44 106
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }*
Plaines_Orientales_Province
	Karundir
	france
	English_Rebels
	162 20 20
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Gaharf_Province
	Gaharf
	sicily
	English_Rebels
	181 108 44
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Gran_Castel_Province
	Gran_Castel
	sicily
	English_Rebels
	243 217 193
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Devrel_Province
	Devrel
	sicily
	English_Rebels
	116 183 184
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Quisan_Province
	Quisan
	sicily
	English_Rebels
	143 135 135
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Haute_Adelie_Province
	Haute_Adelie
	sicily
	English_Rebels
	237 120 34
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Basse_Adelie_Province
	Basse_Adelie
	sicily
	English_Rebels
	24 94 77 
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Thiercelin_Province
	Thiercelin
	sicily
	English_Rebels
	14 143 8
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Da_Bou_Province
	Da_Bou
	sicily
	English_Rebels
	164 229 161
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Milnerethan_Province
	Milnerethan
	milan
	English_Rebels
	136 28 227
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Varnak_Province
	Varnak
	hre
	English_Rebels
	94 75 39
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Valdris_Province
	Valdris
	scotland
	English_Rebels
	35 234 129
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Vantern_Province
	Vantern
	scotland
	English_Rebels
	61 95 107
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Nadantern_Province
	Nadantern
	scotland
	English_Rebels
	189 69 53
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Alastan_Province
	Alastan
	hre
	English_Rebels
	140 95 89
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Nartek_Province
	Nartek
	hre
	English_Rebels
	232 22 22
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Sircus_Province
	Sircus
	hre
	English_Rebels
	241 9 244
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Pentra_Province
	Pentra
	hre
	English_Rebels
	132 253 158
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Nabagreb_Province
	Nabagreb
	hre
	English_Rebels
	133 177 26
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Etheldren_Province
	Etheldren
	milan
	English_Rebels
	121 1 1
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Mordred_Province
	Mordred
	hre
	English_Rebels
	195 81 132
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Korkoron_Province
	Korkoron
	hre
	English_Rebels
	195 11 136
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Bagdanor_Province
	Bagdanor
	hre
	English_Rebels
	79 90 48
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Musthelden_Province
	Musthelden
	milan
	English_Rebels
	31 90 171
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Ridayan_Province
	Ridayan
	scotland
	English_Rebels
	210 200 200
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Mornelande_Province
	Mornelande
	scotland
	English_Rebels
	70 10 10
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Ghomorak_Province
	Ghomorak
	hre
	English_Rebels
	255 116 253
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Deleran_Province
	Deleran
	scotland
	English_Rebels
	99 125 174
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Moclav_Province
	Moclav
	scotland
	English_Rebels
	119 112 112
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Ethieldin_Province
	Ethieldin
	scotland
	English_Rebels
	227 68 251
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Hautemarche_Province
	Hautemarche
	scotland
	English_Rebels
	71 147 144
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Vandassin_Province
	Vandassin
	scotland
	English_Rebels
	91 102 32
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Darbarof_Province
	Darbarof
	hre
	English_Rebels
	234 184 88
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Aldeniel_Province
	Aldeniel
	milan
	English_Rebels
	13 39 88
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Elmourn_Province
	Elmourn
	milan
	English_Rebels
	88 62 13
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Vil_Castel_Province
	Vil_Castel
	milan
	English_Rebels
	203 135 2
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Arhuniroi_Province
	Arhuniroi
	milan
	English_Rebels
	45 203 2
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Daneroth_Province
	Daneroth
	venice
	English_Rebels
	198 203 68
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Lanavan_Province
	Lanavan
	venice
	English_Rebels
	246 74 74
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Haut_Kehlmet_Province
	Nor_Hlmet
	spain
	English_Rebels
	68 203 147
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Bas_Kehlmet_Province
	Ur_Hlmet
	spain
	English_Rebels
	56 100 82
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Valroth_Province
	Valroth
	spain
	English_Rebels
	97 8 8
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Kron_Province
	Kron
	spain
	English_Rebels
	70 8 248
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Nahalroth_Province
	Nahalroth
	england
	English_Rebels
	26 63 95
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Vismes_Province
	Vismes
	england
	English_Rebels
	239 195 171
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Lephandor_Province
	Lephandor
	milan
	English_Rebels
	148 144 45
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Nagareth_Province
	Nagareth
	milan
	English_Rebels
	58 66 128
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Malgaroth_Province
	Malgaroth
	england
	English_Rebels
	126 124 119
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Vil_Uldrinn_Province
	Vil_Uldrinn
	scotland
	English_Rebels
	205 58 200
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Mellusinn_Province
	Mellusinn
	scotland
	English_Rebels
	132 190 224
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Athrapov_Province
	Athrapov
	scotland
	English_Rebels
	255 172 117
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Ker_Abak_Province
	Ker_Abak
	scotland
	English_Rebels
	104 2 59
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Darderan_Province
	Darderan
	scotland
	English_Rebels
	181 144 183
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Tartan_Province
	Tartan
	scotland
	English_Rebels
	183 165 144
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Naharal_Province
	Naharal
	scotland
	English_Rebels
	146 144 183
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
Norhork_Province
	Norhork
	hre
	English_Rebels
	157 183 144
	no_brigands, no_pirates
	5
	6
	religions { catholic 0 orthodox 0 islam 0 pagan 100 heretic 0 }
