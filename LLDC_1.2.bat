@echo off
cd ..\..
IF EXIST kingdoms.exe (start kingdoms.exe @%0\..\lldc.cfg) ELSE (
    echo ERROR: Cannot find the M2TW executable.
    echo You probably installed the mod into the wrong folder.
    pause
  )
)

